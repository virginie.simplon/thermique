<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\CoOwnerShip\Building;
use Faker;

class BuildingFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 1; $i <= 10; $i++)
         {
            $building = new Building();
            $building->setName("Bâtiment n°".$i)
            ->setBuildingNumber($i)
            ->setStair($i)
            ->setTantiemes($faker->numberBetween($min = 8000, $max = 9000))
            ->setTantiemesGarageBox($faker->numberBetween($min = 400, $max = 800))
            ->setTantiemesShop($faker->numberBetween($min = 700, $max = 900))
            ->setOthers($faker->numberBetween($min = 1100, $max = 3000))
            ->setTantiemesHeating($faker->numberBetween($min = 2000, $max = 3000))

;



        $manager->persist($building);
        }
        $manager->flush();
    }
}

