<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\CoOwnerShip\Owner;

class OwnerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i< 10; $i++){
        $owner = new Owner();
        $owner->setGender("test $i");
        $owner->setFirstName("test $i");
        $owner->setLastName("test $i");
        $owner->setCompany("test $i");
        $owner->setPostalCode($i);
        $owner->setCity("test $i");
        $owner->setPhone1("test $i");
            $owner->setPhone2("test $i");
        $owner->setMail("test $i");
        $owner->setFamilyMemberNumber($i);
        $owner->setChildrenNumber($i);
        $owner->setMaritalStatus("test $i");
        $owner->setRetired("test $i");
        $owner->setHousingSituation("test $i");
        $owner->setTaxRevenueOne($i);
        $owner->setTaxRevenueTwo($i);
        $owner->setGotAnah("test $i");
        $owner->setSubAnahAmount($i);
        $owner->setGotTaxCredit($i);
        $owner->setTaxCreditAmount($i);
        $owner->setGotEcoCredit($i);
        $owner->setEcoCreditAmount($i);
        $owner->setCountry("test $i");
        


        $manager->persist($owner);
    }
        $manager->flush();
    }
}
