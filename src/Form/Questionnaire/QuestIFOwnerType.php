<?php

namespace App\Form\Questionnaire;

use App\Entity\CoOwnerShip\Owner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\DBAL\Types\TextType as DoctrineTextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class QuestIFOwnerType extends AbstractType
{
    protected function addOwnerFields(FormBuilderInterface $builder)
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'required' => false,
                'choices' => Owner::$gendersChoices,
                'attr' => [
                    'placeholder' => 'Choisissez...'
                ],
                'label' => 'Genre'
            ] )
            ->add('company', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex: Entreprise XXXX'
                ],
                'label' => 'Société'
            ])
            ->add('firstname', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'ex: Auguste'
                ],
                'label' => 'Prénom'
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'placeholder' => 'ex: Rodin'
                ],
                'label' => 'Nom'
            ])
            ->add('address1', TextType::class, [
                'attr' => [
                    'placeholder' => 'ex: 2, rue des Mimosas'
                ],
                'label' => 'Adresse'
            ])
            ->add('address2', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex: 2ème gauche'
                ],
                'label' => 'Complément adresse'
            ])
            ->add('postalCode', TextType::class, [
                'attr' => [
                    'placeholder' => 'ex: 75000'
                ],
                'label' => 'Code Postal'
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'placeholder' => 'ex: Paris'
                ],
                'label' => 'Ville'
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex: France'
                ],
                'label' => 'Pays'
            ])
            ->add('phone1', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex: 0479000000'
                ],
                'label' => 'Téléphone'
            ])
            ->add('phone2', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex: 0479000000'
                ],
                'label' => 'Téléphone',
                'required' => false
            ])
            ->add('mail', EmailType::class, [
                'attr' => [
                    'placeholder' => 'ex: test@exemple.com'
                ],
                'label' => 'Courriel'
            ])
            ->add('personalData', CheckboxType::class,[
                'required' => false,
//                'attr' => [
//                    'class' => 'switch_1'
//                ],
                'label' => 'J\'ai lu les mentions légales et j\'autorise l\'utilisation de mes données personnelles pour faire la simulation personnelle'
            ])
            ->add('paperCopy', CheckboxType::class,[
                'required' => false,
//                'attr' => [
//                    'class' => 'switch_1'
//                ],
                'label' => 'Je souhaite recevoir une version papier de ma simulation'
            ])
        ;

    }

    protected function addSituationFields(FormBuilderInterface $builder)
    {
        $builder
            ->add('maritalStatus', ChoiceType::class, [
                'choices' => Owner::$maritalsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez une option'],
                'label' => 'Statut marital'
            ])
            ->add('retired', CheckboxType::class, [
                'required' => false,
                'label' => 'Cochez cette case si vous êtes retraité',
//            'attr' => [
//                'class' => 'switch_1'
//            ]
            ])
            ->add('childrenNumber', IntegerType::class, [
                'required' => true,
                'empty_data' => '0',
                'attr' => [
                    'placeholder' => 'ex : 2'
                ],
                'label' => 'Nombre d\'enfants'
            ])
            ->add('housingSituation', ChoiceType::class, [
                'choices' => Owner::$housingsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez une option'
                ],
                'label' => 'Situation'
            ] )
            ->add('taxRevenueOne', NumberType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Exemple: 20527'
                ],
                'label' => 'owner.taxRevenueOne'
            ])
            ->add('taxRevenueTwo',NumberType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Exemple: 20527'
                ],
                'label' => 'owner.taxRevenueTwo'
            ])
            ->add('gotPTZ', CheckboxType::class,[
                'required' => false,
//                'attr' => [
//                    'class' => 'switch_1'
//                ],
                'label' => 'Avez-vous bénéficiez d\'un prêt à Taux Zéro ?'
            ]);
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $step = $options['step'];
        switch($step) {
            default:
            case 1: $this->addOwnerFields($builder); break;
            case 2: $this->addSituationFields($builder); break;
        }
        $builder->add('submit', SubmitType::class, array('label' => 'Enregistrer et suivant'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
            'step' => 1,
            'validation_groups' => ['Default']
        ]);
    }
}
