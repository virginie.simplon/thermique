<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Scenario\WorkHasBuilding;
use App\Entity\CoOwnerShip\Building;
use App\Repository\BuildingRepository;

class WorkHasBuildingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('building', EntityType::class, [
              'class' => Building::class,
              'choice_label' => function(Building $building) {
                return $building->getName();
              },
              'query_builder' => function(BuildingRepository $br) use($options) {
                return $br->findNameByCoOwnershipId($options['coOwnership']);
              }
            ])
            ->add('costHt', NumberType::class, [
              'required' => false,
              'empty_data' =>"0",
                'label' => 'workHasBuilding.costHt'
            ])
            ->add('costTtc', NumberType::class, [
              'required' => false,
              'empty_data' =>"0"
            ])
            ->add('syndicFees', NumberType::class, [
              'required' => false,
              'empty_data' =>"0"
            ])
            ->add('masteryFees', NumberType::class, [
              'required' => false,
              'empty_data' =>"0"
            ])
            ->add('spsFees', NumberType::class, [
              'required' => false,
              'empty_data' =>"0"
            ])
            ->add('technicalFees', NumberType::class, [
              'required' => false,
              'empty_data' =>"0"
            ])
            ->add('insurance', NumberType::class, [
              'required' => false,
              'empty_data' =>"0"
            ])
            ->add('cee', NumberType::class, [
                'required' => false,
                'empty_data' =>"0"
            ])
            ->add('caracValue', NumberType::class, [
                'required' => false,
                'empty_data' =>"0"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['coOwnership']);
        $resolver->setDefaults([
            'data_class' => WorkHasBuilding::class,
        ]);
    }
}
