<?php

namespace App\Form\Document;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class UploadedFileType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        
        $file = $builder->getData(); 
        
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Libellé document',
            ))
            ->add('category', EntityType::class, array(
                'class' => 'App\Entity\Document\DocumentCategory',
                'label' => 'Catégorie',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('file', FileType::class, array(
                'required'      => $file->getId()? false:true,
                'label' => $file->getid()?'Nouveau fichier':'Fichier',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Document\UploadedFile',
//            'attr' => [
//                'data-ajax-form' => '',
//                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
//            ],
//            'from_type' => null,
        ));
    }

}
