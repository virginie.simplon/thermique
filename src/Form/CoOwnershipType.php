<?php

namespace App\Form;

use App\Form\BuildingType;
use App\Entity\CoOwnerShip\CoOwnership;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;

class CoOwnershipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('businessNumber', TextType::class)
            ->add('residenceName', TextType::class)
            ->add('residenceType', TextType::class)
            ->add('address', TextType::class)
            ->add('postalCode', TextType::class)
            ->add('city', TextType::class)
            ->add('department', IntegerType::class)
            ->add('climaticArea', TextType::class)
            ->add('productionType', TextType::class)
            ->add('constructionDate', DateType::class, array(
//                    'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'years' => range(date('Y'), date('Y') - 100),
//                    'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'label' => 'coOwnership.constructionDate',
                'required' => false
            ))
            ->add('coOwnershipFragile', CheckboxType::class, [
                'required' => false
            ])
            ->add('worksFund', IntegerType::class)
            ->add('surveyOpeningDate', DateType::class)
            ->add('surveyClosingDate', DateType::class)
            ->add('numberOfHousings', IntegerType::class)
            ->add('livingSpace', IntegerType::class)
            ->add('numberOfShops', IntegerType::class, array(
                'empty_data' => 0
            ))
            ->add('numberOfOffices', IntegerType::class)
            ->add('livingSpaceAverage', IntegerType::class)
            ->add('projectFees', PercentType::class, array('scale' => 2, 'type' => 'integer'))
            ->add('auditInitialDate', DateType::class)
            ->add('auditDoneByOffice', CheckboxType::class, [
                'required' => false
            ])
            ->add('energyInitialTag', TextType::class)
            ->add('exonerationPropertyTax', CheckboxType::class, [
                'required' => false
            ])
            ->add('tantiemesTotal', IntegerType::class)
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CoOwnership::class,
        ]);
    }
}
