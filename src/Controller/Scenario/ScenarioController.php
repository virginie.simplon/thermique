<?php

namespace App\Controller\Scenario;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\Scenario\Scenario;
use App\Form\ScenarioType;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class ScenarioController extends AbstractController
{
    /**
     * @Route({
     *  "en": "/scenarios",
     *  "fr": "/scenarios"
     * }, name="scenarios_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership, int $maxItemsPerPage = 15): Response
    {
        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $paginator->paginate(
            $repository->findScenariosByCoOwnership($coOwnership),
            $request->query->getInt('page', 1),
            $maxItemsPerPage
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'scenarios' => $scenarios,
            'object' => 'scenario'
        ]);
    }
    
  /**
   * @Route({
   *  "en": "/scenario/add",
   *  "fr": "/scenario/ajouter"
   * }, name="scenario_new")
   */
  public function addScenario(Request $request, CoOwnership $coOwnership)
  {
      $scenario = new Scenario();
      $form = $this->createForm(ScenarioType::class, $scenario, [
        'coOwnership' => $coOwnership,
      ]);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($scenario);

          foreach($scenario->getWorkHasScenarios() as $workHasScenarios) {
            $workHasScenarios->setScenario($scenario);
          }

          $em->flush();

          return $this->redirectToRoute('scenario_new', ['referenceId' => $coOwnership->getId()]);
      }

      return $this->render('coOwnership/scenario/new.html.twig', [
          'reference' => $coOwnership,
        'form' => $form->createView(),
          'object' => 'scenario'
      ]);
  }

    /**
     * @Route({
     *  "en": "/scenario/{id}",
     *  "fr": "/scenarios/{id}"
     * }, name="scenario_show", methods={"GET"})
     */
    public function show(Scenario $scenario, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $scenario,
            'object' => 'scenario'
        ]);
    }

    /**
     * @Route({
     *  "en": "/scenario/{id}/edit",
     *  "fr": "/scenarios/{id}/modifier"
     * }, name="scenario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Scenario $scenario, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(ScenarioType::class, $scenario, [
            'coOwnership' => $coOwnership,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('scenarios_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'entity' => $scenario,
            'form' => $form->createView(),
            'object' => 'scenario'
        ]);
    }

    /**
     * @Route({
     *  "en": "/scenario/{id}/delete",
     *  "fr": "/scenarios/{id}/supprimer"
     * }, name="scenario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Scenario $scenario, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$scenario->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($scenario);
            $entityManager->flush();
        }

        return $this->redirectToRoute('scenarios_index', ['referenceId' => $coOwnership->getId()]);
    }
}
