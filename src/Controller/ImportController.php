<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\Export\Upload;
use App\Form\UploadType;
use App\Service\Import;
use App\Service\Reader;
use App\Service\Parser;

/**
 * @Route({
 *  "en": "/admin/project/{id}",
 *  "fr": "/admin/projet/{id}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"id" = "id"}})
 */

class ImportController extends AbstractController
{



}
