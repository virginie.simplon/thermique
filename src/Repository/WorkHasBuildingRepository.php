<?php

namespace App\Repository;

use App\Entity\Scenario\Scenario;
use App\Entity\Scenario\WorkHasBuilding;
use App\Entity\Scenario\Work;
use App\Entity\CoOwnerShip\Building;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WorkHasBuilding|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkHasBuilding|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkHasBuilding[]    findAll()
 * @method WorkHasBuilding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkHasBuildingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WorkHasBuilding::class);
    }

    // /**
    //  * @return WorkHasBuilding[] Returns an array of WorkHasBuilding objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WorkHasBuilding
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    protected function getQbWorkHasBuilding(Work $work = null, Building $building = null, Scenario $scenario = null)
    {
        $qb = $this->createQueryBuilder('whb');
        if($scenario) {
            $qb->leftJoin('whb.work', 'w')
                ->leftJoin('w.workHasScenarios', 'ws')
                ->andWhere('ws.scenario = :scenario')
                ->setParameter('scenario', $scenario);
        }
        if($work) {
            $qb->andWhere('whb.work = :work')
                ->setParameter('work', $work);
        }
        if($building) {
            $qb->andWhere('whb.building = :building')
                ->setParameter('building', $building);
        }
        return $qb;
    }

    public function findWorkHasBuildingsByBuilding(Building $building) {
        return $this->getQbWorkHasBuilding(null, $building)->getQuery();
    }

    public function findByBuildingScenario(Building $building, Scenario $scenario) {
        return $this->getQbWorkHasBuilding(null, $building, $scenario)->getQuery();
    }

    public function getWorkHasBuilding(Work $work, Building $building)
    {
        $qb = $this->getQbWorkHasBuilding($work, $building);
        $res = $qb->getQuery()
            ->getResult();

        if(count($res) > 0) {
            return $res['0'];
        }
        return null;
    }
}
