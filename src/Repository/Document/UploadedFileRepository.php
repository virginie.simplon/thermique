<?php

namespace App\Repository\Document;

use Doctrine\ORM\EntityRepository;

/**
 * UploadedFileRepository
 *
 */
class UploadedFileRepository extends EntityRepository
{
    public function findAllDeletedFiles(\Datetime $aDate)
    {   
        return $this->createQueryBuilder('f')
            ->where('date(f.deletedAt) <= :aDate')
            ->setParameter(':aDate', $aDate)
            ->getQuery()
            ->getResult();   
    }

    public function findCoOwnershipFile($coOwnership_id, $file_id)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.coOwnership', 'g')
            ->andWhere('g.id = :coOwnership_id')
            ->andWhere('f.id = :file_id')
            ->setParameter(':coOwnership_id', $coOwnership_id)
            ->setParameter(':file_id', $file_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findDocuments($filters)
    {
        $qb =  $this->createQueryBuilder('f')
            ->andWhere('f.coOwnership IS NOT NULL')
            ->orderBy('f.createdAt', 'DESC');

        if (isset($filters['title'])){
            $qb->andWhere('f.title like :title')
                ->setParameter('title', '%'.$filters['title'].'%');
        }

        if (isset($filters['coOwnership'])){
            $qb->andWhere('f.coOwnership = :coOwnership')
                ->setParameter('coOwnership', $filters['coOwnership']);
        }

        if (isset($filters['category'])){
            $qb->andWhere('f.category = :category')
                ->setParameter('category', $filters['category']);
        }


        return $qb;
    }

    public function queryLastFiles($limit = 5, $recent = false)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->orderBy('f.createdAt', 'DESC')
            ->setMaxResults($limit);
        if($recent) {
            $date = new \DateTime();
            $date->sub(new \DateInterval("P3M")); // check info sooner than 3 month
            $qb->where('f.createdAt > :date')
                ->setParameter('date', $date);
        }

        return $qb;
    }
 
}
