<?php

namespace App\Repository\Document;

use App\Entity\Document\DocumentCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentCategory[]    findAll()
 * @method DocumentCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParameterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DocumentCategory::class);
    }

    // /**
    //  * @return Grant[] Returns an array of Grant objects
    //  */

    public function queryDocuments()
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.setting = :val')
            ->setParameter('val', DocumentCategory::DOC_TYPE_DOCUMENT)
            ->orderBy('d.name', 'ASC')
        ;
    }

    /*
    public function findOneBySomeField($value): ?Grant
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
