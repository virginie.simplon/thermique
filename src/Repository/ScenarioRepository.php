<?php

namespace App\Repository;

use App\Entity\Scenario\Scenario;
use App\Entity\CoOwnerShip\CoOwnership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Scenario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Scenario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Scenario[]    findAll()
 * @method Scenario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScenarioRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Scenario::class);
    }

    public function findScenariosByCoOwnership(CoOwnership $coOwnership) {
      $qb = $this->createQueryBuilder('s')
            ->addSelect('ws, w, wb, b')
            ->leftJoin('s.workHasScenarios', 'ws')
            ->leftJoin('ws.work', 'w')
            ->leftJoin('w.workHasBuildings', 'wb')
            ->leftJoin('wb.building', 'b')
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership)
            ->getQuery();

      return $qb;
    }


    // /**
    //  * @return Scenario[] Returns an array of Scenario objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Scenario
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
