<?php

namespace App\Service\User;

use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\User\User;
use FOS\UserBundle\Model\UserManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;


class Generator
{

  private $coOwnership;
  private $coOwnershipName;
  private $passwordEncoder;
  private $manager;
  private $userManager;

  public function __construct(EntityManager $manager, UserPasswordEncoderInterface $passwordEncoder, UserManagerInterface $userManager) {
      $this->passwordEncoder = $passwordEncoder;
      $this->manager = $manager;
      $this->userManager = $userManager;
  }

  protected function setCoOwnership(CoOwnership $coOwnership)
  {
      $this->coOwnership = $coOwnership;
      $name = substr($this->coOwnership->getResidenceName(), 0, 4);
      $this->coOwnershipName = str_replace(' ', '', $name);
  }

  public function generateUsername(Owner $owner) {
    return sprintf("%s-%05d", $this->coOwnershipName, $owner->getId());
  }

  public function randomPassword(int $length = 8) {
    $chars =  'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';

    $str = '';
    $max = strlen($chars) - 1;

    for($i = 0; $i < $length; $i++) {
      $str .= $chars[random_int(1, $max)];
    }

    return $str;
  }

  protected function createResult($owner, $user, $password)
  {
      $result = array();
      $result['owner'] = $owner;
      $result['account'] = $user->getUsername();
      $result['password'] = $password;
      return $result;
  }

  protected function createUser($owner)
  {
      $user = new User();
      $user->setUsername($this->generateUsername($owner));
      $user->setEmail($owner->getMail());
      $user->setEnabled(true);
      $user->setSalt(null);
      $password = $this->setNewPassword($user);
      $user->setPasswordRequestedAt(null);
      $user->setLastLogin(null);
      $user->setConfirmationToken(null);
      $user->setRoles(array());
      $user->setOwner($owner);
      $this->manager->persist($user);
      return $this->createResult($owner, $user, $password);
  }

  protected function setNewPassword(&$user)
  {
      $password = $this->randomPassword();
      $user->setPlainPassword($password);
      return $password;
  }

    protected function setOwnerNewPassword($owner)
    {
        $user = $owner->getUser();
        $password = $this->setNewPassword($user);
        $this->userManager->updateUser($user, true);
        return $this->createResult($owner, $owner->getUser(), $password);
    }


    public function generateUser(CoOwnership $coOwnerShip, $owners) {
      $this->setCoOwnership($coOwnerShip);
       $results = array();
        foreach($owners as $owner) {
            if($owner->getUser()) {
                $results[] = $this->setOwnerNewPassword($owner);
            } else {
                $results[] = $this->createUser($owner);
            }
            $this->manager->flush();
        }
        return $results;
    }
}
