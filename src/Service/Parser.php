<?php

namespace App\Service;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\CoOwnerShip\Lot;
use App\Entity\CoOwnerShip\Building;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\Scenario\Scenario;
use App\Entity\Scenario\Work;
use App\Entity\Scenario\WorkHasBuilding;
use App\Entity\Scenario\WorkHasScenario;
use Doctrine\ORM\EntityManager;
use App\Service\Validation;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Parser {

  private $spreadsheet = [];
  private $validation;
  private $type;
  private $header;

  public function __construct(EntityManager $manager, ValidatorInterface $validator, UserPasswordEncoderInterface $passwordEncoder) {
    $this->validation = new Validation($manager, $validator, $passwordEncoder);
    $this->header = array();
  }

  public function getHeader()
  {
      return $this->header;
  }


  protected function fillHeader($data)
  {
      foreach ($data as $value) {
          $this->header[] = strtolower($value);
      }
  }

  protected function getOwnerValue($data, $field)
  {
      $value = $data[$field];

      if($value) {
          $test = strtolower($value);
          switch ($test) {
              case 'oui':
                  $value = true;
                  break;
              case 'non':
                  $value = false;
                  break;
              case 'propriétaire bailleur':
              case 'bailleur':
                  $value = Owner::HOUSING_LESSOR;
                  break;
              case 'propriétaire occupant':
              case 'occupant':
                  $value = Owner::HOUSING_USE;
                  break;
              case "locataire":
                  $value = Owner::HOUSING_RENT;
                  break;
              case "loger gratuitement":
                  $value = Owner::HOUSING_FREE;
                  break;
              case "célibataire ou veuvage":
                  $value = Owner::MARITAL_SINGLE;
                  break;
              case (preg_match('/couple sans*/', $test) ? true : false) :
                  $value = Owner::MARITAL_COUPLE;
                  break;
              case (preg_match('/couple avec*/', $test) ? true : false) :
                  $value = Owner::MARITAL_PARENTS;
                  break;
              case "parent isolé":
              case "vous vivez seul(e) avec enfants":
                  $value = Owner::MARITAL_SINGLE_PARENT;
                  break;
          }
      }
      return $value;
  }

    protected function ownerSend(CoOwnership $coOwnership)
    {
        $buildingRepo = $this->validation->getRepository(Building::class);
        $lotRepo = $this->validation->getRepository(Lot::class);

        foreach ($this->spreadsheet as $line => $data) {
            $name = $data['building'];
            if (!$buildingRepo->isBuildingExist($coOwnership, $name)) {
                $this->insertBuildingValues($coOwnership, $name, $line);
                $this->validation->flush();
            }
        }


        foreach ($this->spreadsheet as $line => $data) {


            // search/create lot and owner
            $number = $data['lot'];
            $lot = $lotRepo->getLotByNumber($coOwnership, $number);
            $owner = null;
            $persistLot = false;
            $persistOwner = false;
            if (!$lot) {
                $lot = new Lot();
                $owner = new Owner();
                $persistLot = true;
                $persistOwner = true;
            } else {
                $owner = $lot->getFirstOwner();
                if (!$owner) {
                    $owner = new Owner();
                    $persistOwner = true;
                }
            }
            $building = $buildingRepo->getBuildingByName($coOwnership, $data['building']);
            $lot->setBuilding($building);
            $lot->setNumber((string)$data['lot']);

            // fill general fields
            foreach($this->header as $field) {
                $value = $this->getOwnerValue($data, $field);
//                if($field == "TantiemesCellar") {
//                    dump($line);
//                    dump($value);
//                    dump($this->getHeader());
//                    dump($data);
//                    die;
//                }
                switch($field) {
                    case 'building':case 'lot':
                        break;
//                    case 'roomArea': if($value) {dump($field); dump($value);die;}
                    default:
                        $this->addValue($lot, $field, $value, $line);
                        $this->addValue($owner, $field, $value, $line);
                        break;
                }
            }

            // persist if necessary
            if ($persistLot) {
                $this->validation->persist(Validation::LOT, $lot, $line);
            }
            if ($persistOwner) {
                $owner->addLot($lot);
                $this->validation->persist(Validation::OWNER, $owner, $line);
            }

        }
    }

  /* work */
    private function insertWorkHasBuilding($work, $building, $force, $data, $line)
    {
        $persist = false;
        $wHB = null;
        if(!$force) {
            $workHasBuildingRepo = $this->validation->getRepository(WorkHasBuilding::class);
            $wHB = $workHasBuildingRepo->getWorkHasBuilding($work, $building);
        }
        if(!$wHB) {
            $wHB = new WorkHasBuilding();
            $wHB->setWork($work);
            $wHB->setBuilding($building);
            $persist = true;
        }
        foreach($this->header as $field) {
            $value = $data[$field];
            switch($field) {
                case 'work':
                case 'building':
                    break;
                default:
                    $this->addValue($wHB, $field, $value, $line);
                    break;
            }
        }
        if($persist) {
            $this->validation->persist(Validation::WORK, $wHB, $line);
        }
        return $wHB;
    }

    protected function workSend(CoOwnership $coOwnership)
    {
        $buildingRepo = $this->validation->getRepository(Building::class);
        $workRepo = $this->validation->getRepository(Work::class);
        $force = false;

        foreach($this->spreadsheet as $line => $data) {
//            dump($data);die;
            $buildingName = $data['building'];
            $building = $buildingRepo->getBuildingByName($coOwnership, $buildingName);
            if(!$building) {
                $building = $this->insertBuildingValues($coOwnership, $buildingName, $line);
                $this->validation->flush();
                $force = true;
            }

            $workName = $data['work'];
            $work = $workRepo->getWorkByName($coOwnership, $workName);
            if(!$work) {
                $work = $this->insertWorkValues($workName, $line);
                $this->validation->flush();
                $force = true;
            }

            $wHB = $this->insertWorkHasBuilding($work, $building, $force, $data, $line);
            $this->validation->flush();
        }
    }

    /* scenario */

    protected function get_method($object, $method){
        if(!method_exists($object, $method)) {
            return null;
        }
        return function() use($object, $method){
            $args = func_get_args();
            return call_user_func_array(array($object, $method), $args);
        };
    }
    
    protected function addValue($object, $field, $value, $line)
    {
//        if($field == "TantiemesCellar" and $value == 'couple') {
//            dump($line);
//            dump($field);
//            dump($value);
//            dump($line);
//            die;
//        }
        try {
            if ($value) {
                $func = $this->get_method($object, 'set' . $field);
                if ($func) {
                    $func($value);
                }
            }
        } catch (\Exception $e) {

        }
    }

    protected function addScenarioLine($line, $data, $coOwnership)
    {
        $scenario = new Scenario();

        foreach($this->header as $field) {
            $value = $data[$field];
            switch($field) {
                case (preg_match('/work.*/', $field) ? true : false) :
                    break;
                default:
                    $this->addValue($scenario, $field, $value, $line);
                    break;
            }
        }
        $this->validation->persist(Validation::SCENARIO, $scenario, $line);
        foreach($this->header as $value) {
            $res = $data[$value];
            switch($value) {
                case (preg_match('/work.*/', $value) ? true : false) :
                    if($res and $res != '') {
                        $workRepo = $this->validation->getRepository(Work::class);
                        $work = $workRepo->getWorkByName($coOwnership, $res);
                        if ($work) {
                            $this->insertWorkHasScenario($work, $scenario, true, $line);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        $this->validation->flush();
    }

    protected function scenarioSend(CoOwnership $coOwnership)
    {
        foreach($this->spreadsheet as $line => $data) {
            $this->addScenarioLine($line, $data, $coOwnership);
        }
    }

    protected function objectParse(array $datas)
    {
        $first = true;

        foreach($datas as $index => $data) {
            if($data != null) {
                if($first) {
                    $this->fillHeader($data);
                    $first = false;
                } else {
                    $this->spreadsheet[$index] = [];
                    foreach ($data as $i => $value) {
                        $this->spreadsheet[$index][$this->header[$i]] = $data[$i];
                    }
                }
            }
        }
//        dump($this->getHeader());die;
    }
  /**
   * Parse spreadsheet and transform in array
   *
   * @param Array
   * @return null
   */

  public function parse(array $datas, $type) {
      $this->type = $type;
      return $this->objectParse($datas);
  }

  /**
   * Send infos in the table Building, Lot and Owner
   *
   * @param id Integer
   * @return App\Service\Validation
   */

  public function send(CoOwnership $coOwnership) {

      switch($this->type) {
          case 'owner': $this->ownerSend($coOwnership); break;
          case 'work': $this->workSend($coOwnership); break;
          case 'scenario': $this->scenarioSend($coOwnership); break;
          default: return null;
      }
    $this->validation->flush();

    return $this->validation;
  }

  /**
   * Reset spreadsheet parser
   * @return null
   */

  public function reset() {
    reset($this->spreadsheet);
  }

  /**
   * Get the spreadsheet parser
   * @return array
   */

  public function getSpreadsheet() {
    return $this->spreadsheet;
  }

  /**
   * Insert Buidlding values in the table
   *
   * @param Building
   * @param CoOwnership
   * @param String
   * @param Validation
   * @return null
   */

  private function insertBuildingValues(CoOwnership $coOwnership, $name, $line) {
    $building = new Building();
    $building->setCoOwnership($coOwnership);
    $building->setName((string) $name);

    $this->validation->persist(Validation::BUILDING, $building, $line);
    return $building;
  }

    private function insertWorkValues($name, $line) {
        $work = new Work();
        $work->setName((string) $name);

        $this->validation->persist(Validation::BUILDING, $work, $line);
        return $work;
    }

    private function insertWorkHasScenario($work, $scenario, $force, $line)
    {
        $persist = false;
        $wHS = null;
        if(!$force) {
            $workHasScenarioRepo = $this->validation->getRepository(WorkHasScenario::class);
            $wHS = $workHasScenarioRepo->getWorkHasScenario($work, $scenario);
        }
        if(!$wHS) {
            $wHS = new WorkHasScenario();
            $wHS->setWork($work);
            $wHS->setScenario($scenario);
            $persist = true;
        }
        if($persist) {
            $this->validation->persist(Validation::SCENARIO, $wHS, $line);
        }
        return $wHS;
    }

  /**
   * Insert Lot values in the table
   *
   * @param Lot
   * @param Building
   * @param Array
   * @param Validation
   * @return null
   */

  private function insertLotValues(Lot $lot, Owner $owner, ?Building $building, array $data, $line) {

  }

  /**
   * Insert Owner values in the table
   *
   * @param Owner
   * @param Lot
   * @param Array
   * @param Validation
   * @return null
   */

  private function insertOwnerValues(Owner $owner, Lot $lot, array $data, $line) {
    $owner->setGender((string) $data['gender']);
    $owner->setLastname((string) $data['lastname']);
    $owner->setFirstname((string) $data['firstname']);
    $owner->setCompany((string) $data['company']);
    $owner->setAddress1((string) $data['address1']);
    $owner->setAddress2((string) $data['address2']);
    $owner->setPostalCode((string) $data['postalCode']);
    $owner->setCity((string) $data['city']);
    $owner->setCountry((string) $data['country']);
    $owner->setPhone1((string) $data['phone1']);
    $owner->setPhone2((string) $data['phone2']);
    $owner->setMail((string) $data['mail']);
    $owner->addLot($lot);

    $this->validation->persist(Validation::OWNER, $owner, $line);
  }

}
